# /home/damien/xylnea/_cor/:dkr/>0.0.1
from pprint import pprint
from subprocess import run
from threading import Thread
from datetime import datetime
#
class xylnea_launch_infra(Thread):
  #
  def __init__(self):
    Thread.__init__(self)
    self.routines = {
      '`> !echo_populate': f'echo python xla_populate_{"spi"}_{"pgdb"}_{"envs"}.py',
      '`> !run_populate': f'python xla_populate_{"spi"}_{"pgdb"}_{"envs"}.py',
      #'`> !run_populate': f'echo "python xla_restore_archive:{'0.0.1'}.py"'
    }
    self.log = {}
    #pprint(self)
  #
  def run(self):
    #
    class __run__thread__(Thread):
      def __init__(self, cmd:str):
        Thread.__init__(self)
        self.cmd = cmd
        self.log = run(self.cmd, shell=True, check=True)
      #   print(self)
      # def run(self):
    ##     
    for log, cmd in self.routines.items():
      now = datetime.now()
      shcmd = __run__thread__(cmd)
      shcmd.start()
      shcmd.join()
      # pprint(shcmd.__dict__)
      self.log[now] = {
        '__thread__': shcmd,
        '__stdout__': shcmd.__dict__['log'].__dict__['args']
      }
      # pprint(f'{log} {self.log[now]["__stdout__"]} {now.isoformat()} [ start() ]')
    # for th in self.log.values():
      # pprint(f'{log} {self.log[now]["__stdout__"]} {now.isoformat()} [ join() ]')

xli = xylnea_launch_infra()
xli.start()
xli.join()
print(f'DONE')