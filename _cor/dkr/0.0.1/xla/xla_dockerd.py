from subprocess import run
from threading import Thread
from datetime import datetime
#
class xylnea_launch_infra(Thread):
  #
  def __init__(self):
    Thread.__init__(self)
    self.routines = {
      '`> !run_dockerd' : 'sudo dockerd',
    }
    self.log = {}
    print(self)
  #
  def run(self):
    #
    class __run__thread__(Thread):
      def __init__(self, cmd:str):
        Thread.__init__(self)
        self.cmd = cmd
        self.log = run(self.cmd, shell=True, check=True)
      #   print(self)
      # def run(self):
    ##     
    for log, cmd in self.routines.items():
      now = datetime.now()
      shcmd = __run__thread__(cmd)
      shcmd.start()
      shcmd.join()
      print(shcmd.__dict__)
      self.log[now] = {
        '__thread__': shcmd,
        '__stdout__': shcmd.__dict__['log'].output
      }
      print(f'{log} {self.log[now].__stdout__} {self.log[now].__stdout__} {now.isoformat()} [\tstart()\t]')
    for th in self.log.values():
      print(f'{log} {self.log[now].__stdout__} {self.log[now].__stdout__} {now.isoformat()} [\tjoin()\t]')

xli = xylnea_launch_infra()
xli.start()
xli.join()